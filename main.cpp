#include "DisplayBuffer.h"
#include "WProgram.h"

// set the PWM output pin
// The Arduino pin9 is...
//    connected to Atmega328 pin 15 (see Arduino schematic), which ...
//    is named PB1 (OC1A/PCINT1), which...
//    is the Timer1/Counter1 output pin, see datasheet Chapter 15 (pg 113)
int pwmPin = 9;

// the OCR1A value (range from 1 to 254)
// this value sets the pulse width
static int ocr1a = 125;

// the LCD buffer
static DisplayBuffer db;

//===========================
// display the current OCR1A value on the 2nd line of the display
void show(int val)
  {
  char buf[15];
  sprintf(buf, "OCR1A : %d", ocr1a);
  db.setLine2(buf);

  // show the buffer contents on the LCD
  db.send();
  }

//===========================
// check the range and set the OCR1A value
void set(int& val)
  {
  // PWM only works with OCR1A values between 1 and 254 inclusive
  if (val > 254)
    {
    val = 254;
    }
  else if (val < 1)
    {
    val = 1;
    }

  // set the OCR1A value on the pin
  analogWrite(pwmPin, val);
  }

//===========================
void setup()
  {
  // LCD communicates at 4800
  Serial.begin(4800);

  // clear the screen
  db.clear();

  // set the first line to show we are starting up
  db.setLine1("Starting v0.01");

  // send the buffer contents to the LCD
  db.send();

  // make sure the user can see it for a few seconds
  delay(3000);

  // From the init() function:
  //   sbi(TCCR0B, CS01);
  //   sbi(TCCR0B, CS00);
  //   sbi(TCCR1A, WGM10);
  //
  // which sets the timer to Mode 1, see datasheet pg 136:
  //                                    TOP    setOCR1x    TOV1 flag
  // 0 0 0 1 PWM, Phase Correct, 8-bit 0x00FF     TOP      BOTTOM

  // The behavior for Mode 1 is:
  //    start at BOTTOM=0x00
  //    TOP is 0xFF = 255
  //    count up at sysclock/divisor rate until xx, set OC1A to 0
  //    continue to count up until TOP
  //    count down at sysclock/divisor rate until xx, set OC1A to 1
  //    continue to count down until BOTTOM
  //    repeat

  // when OCR1A set to 125 should see a pulse on pin 9 with
  //    on time  => 1000uS
  //    off time => 1040uS off time
  // with a PWM frequency of 490Hz
  //
  // Here is the calculation for OCR1A values:
  //   sysclock     => 16Mhz => 16000000
  //   divisor      => 64, this is the default (setting is 0x03)
  //   clock freq   => 16Mhz / divisor  => 16Mhz / 64 => 250Khz
  //   clock period => 1 / clock freq   => 1 / 250Khz => 4uS
  //   PWM freq     => clock freq / 510 => 250Khz / 510  = 490Hz
  //                => note the 510 is 2 * 255
  //                => the 2 is because the count occurs both in the up and the down direction
  //                => the 255 is because the clock period is not used up when
  //                => the match of OCR1A occurs (otherwise 256 would have been used)
  //   PWM period   => 1 / 490Hz  => 2040uS
  //
  //   Pulse On time => 2 * 125 * clock period = 2 * 125 * 4uS = 1000uS
  //   Pulse Off time=> 2 * (255 - 125) * clock period = 2 * (255-125) * 4uS = 1040uS
  //                 => the "2" is used since the timer counts up and down for the on and off times
  //   Total Pulse   => 1000 + 1040 => 2040uS

  ocr1a = 125;
  set(ocr1a);
  show(ocr1a);
  }

//===========================
void loop()
  {
  // keep track of the led state
  static bool ledstate = false;

  // flash the led
  ledstate = !ledstate;
  if (ledstate)
    {
    db.ledOn();
    }
  else
    {
    db.ledOff();
    }

  int prev = ocr1a;

  // check if the user pressed a button
  if (Serial.available())
    {
    // the user pressed a button, so go get it
    char key = Serial.read();
    if ('1' == key)
      {
      ocr1a++;
      }
    else if ('4' == key)
      {
      ocr1a--;
      }
    else
      {
      // don't understand the button press, tell the user
      db.beep();
      }
    }

  // if there has been a change in the ocr1a value, display it
  if (ocr1a != prev)
    {
    set(ocr1a);
    show(ocr1a);
    }

  delay(100);
  }

//===========================
int main()
  {
  init();
  setup();
  for (;;)
    {
    loop();
    }
  return 0;
  }
